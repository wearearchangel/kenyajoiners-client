$(document).ready ->
	$('.hero-slider').slick
		autoplay: true,
		fade: true,
		cssEase: 'linear'
		dots: false,
		arrows: false

	$('.hero-slider-single').slick

	$('.hero-slider-scrollable').slick
		slidesToShow: 3,
		slidesToScroll: 3

	$(".is-content-scrollable").niceScroll({touchbehavior:false,cursorcolor:"#aaa",cursoropacitymax:1,cursorwidth:6,cursorborder:"0 solid #2848BE",cursorborderradius:"0",background:"rgba(0,0,0,.15)",autohidemode:"false"})
